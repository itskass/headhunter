<img src="https://gitlab.com/itskass/headhunter/raw/master/logo.png" height=128 width=128>

# HEADHunter

HEADHunter is a command line tool for downloading ethereum blockchains into MongoDB using the JSON RPC. HEADHunter can listen for new blocks and implements basic synchronization. Suitable for the Ethereum Mainnet as well as private Ethereum networks.

```
headhunter <global-flags> command <command-flags>
```

## Contents
- [Installation](#installation)
- [Quick Start](#quick-start)
- [Documentation](#documentation)

## Installation:
You have a few ways to install the headhunter command line tool:

### Install using go:
```shell
go get gitlab.com/itskass/headhunter
# test it was installed
headhunter --help
```

### Install using docker:
```shell
docker pull registry.gitlab.com/itskass/headhunter:latest
docker tag registry.gitlab.com/itskass/headhunter:latest headhunter
# test it was install
docker run headhunter --help
```


## Quick Start:
Headhunter requires a MongoDB instance to connect to as well the RPC endpoint of a node (often at port 8545).
The following subscribe command can be used fully download the current blockchain and keep it synchronized:
```
headhunter --rpc <rpc-url> --db <db-url> subscribe --connect --sync
```

For more help see [the documentation](#documentation) below.

## Documentation
- [Commands and flags](./docs.md)
- [Examples](./examples.md)
