package cmds

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"gitlab.com/itskass/headhunter/schema"

	"github.com/urfave/cli"

	"github.com/globalsign/mgo/bson"
	"gitlab.com/itskass/headhunter/gather"
	"gitlab.com/itskass/headhunter/helpers"
)

var _gatherDescription = `
	The gather command is often used to gather specific blocks or to synchronize a the 
	blockchain up to a given point.

	GATHER TARGET
		The target is the block(range) gather will download. If no target is provided gather will
		will retrieve the latest block, otherwise you should provide either a number, number range
		or hash (as hexidecimal). e.g. '--number 55' will retrieve block 55.

	SYNCHRONIZING
		Synchronization will find and download all missing blocks up to the target. If you provided
		a target block with number 31, then all missing blocks between 0-31 will be downloaded.

	LOGGING
		HEADHunter aims to only log relevant information based on your usage, but you can silence 
		most logs with the '--silent' flag or show more logs with the '--verbose' flag.
`

var Gather = cli.Command{
	Name:        "gather",
	Usage:       "gather downloads specified the blocks. If no blocks are specified the latest block is used as a target",
	Description: _gatherDescription,
	Flags: []cli.Flag{
		&cli.Uint64Flag{
			Name:  "number",
			Usage: "target block by number (index)",
		},
		&cli.StringFlag{
			Name:  "range",
			Usage: "target blocks in the given range <start:end>",
		},
		&cli.StringFlag{
			Name:  "hash",
			Usage: "target block via hash",
		},
		&cli.BoolFlag{
			Name:  "receipts",
			Usage: "requests and save receipts for transactions",
		},
		&cli.BoolFlag{
			Name:    "connect",
			Usage:   "connects target to known ancestor, by downloading the missing blocks",
			EnvVars: []string{"HH_CONNECT"},
		},
		&cli.BoolFlag{
			Name:    "sync",
			Usage:   "gather missing blocks",
			EnvVars: []string{"HH_SYNC"},
		},
		&cli.BoolFlag{
			Name:  "verbose",
			Usage: "show all log outputs when synchronizing",
		},
		&cli.BoolFlag{
			Name:  "silent",
			Usage: "silences all logs",
		},
	},
	Action: _gather,
}

func _gather(c *cli.Context) error {

	// set logging options
	if !c.Bool("verbose") && c.Bool("sync") {
		gather.Log.SetFlags(0)
		gather.Log.SetOutput(ioutil.Discard)
	}
	if c.Bool("silent") {
		gather.Log.SetFlags(0)
		gather.Log.SetOutput(ioutil.Discard)
		gather.SyncLog.SetFlags(0)
		gather.SyncLog.SetOutput(ioutil.Discard)
	}

	// create gather options
	opts := &gather.Options{
		DB:                helpers.DB(c.String("db")),
		Client:            helpers.Client(c.String("rpc")),
		GetAncestors:      c.Bool("connect"),
		GetReceipts:       c.Bool("receipts"),
		ShouldSynchronize: c.Bool("sync"),
		Schema:            schema.Get(c.String("schema")),
	}

	// gather target
	if c.IsSet("number") {
		opts.Target = bson.M{"number": c.Uint64("number")}
	} else if c.IsSet("hash") {
		opts.Target = bson.M{"hash": c.String("hash")}
	} else {
		opts.Target = bson.M{"latest": "true"}
	}

	// run range
	if c.IsSet("range") {
		log.Println("downloading range:")
		start, end := parseRange(c.String("range"))
		for i := start; i < end; i++ {
			opts.Target = bson.M{"number": uint64(i)}
			opts.GetAncestors = false      // cant get ancestors for range
			opts.ShouldSynchronize = false // cant synchronize for rane
			gather.Blocks(opts)
		}
		return nil
	}

	// run default
	gather.Blocks(opts)
	return nil
}

func parseRange(str string) (int, int) {
	s := strings.Split(str, ":")
	if len(s) != 2 {
		log.Fatal("bad range:", str)
	}
	start, _ := strconv.Atoi(s[0])
	end, _ := strconv.Atoi(s[1])
	return start, end
}
