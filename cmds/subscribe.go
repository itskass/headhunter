package cmds

import (
	"io/ioutil"
	"log"
	"time"

	"gitlab.com/itskass/headhunter/schema"
	"gitlab.com/itskass/headhunter/subscribe"

	"github.com/urfave/cli"
	"gitlab.com/itskass/headhunter/gather"
	"gitlab.com/itskass/headhunter/helpers"
)

var _subscribeDescription = `
	Subscribe is used to listen for and download the latested announced headers.

	UPDATE FREQUENCY:
		By default subscribe will request the latest block from the connected node once every
		30 seconds, you can use the '--delay' flag to change this update period.

	PREVENTING GAPS:
		When using HTTP RPC, subscribe will make a request for the latest block periodically,
		Use the '--connect' flag to retrieve all the blocks mined since the last periodic request.
		Without the '--connect' flag you will only recieve the most recent block in the period and 
		are likely to miss intermediate blocks in the sequence. 

	SYNCHRONIZING:
		Synchronization will find and download all missing blocks up to the latest block.
		To ensure you download the complete blockchain and keep it updated. use the '--sync' flag 
		with and the '--connect' flag. e.g. 'subscribe --connect --sync'
	
	LOGGING
		HEADHunter aims to only log relevant information based on your usage, but you can silence 
		most logs with the '--silent' flag or show more logs with the '--verbose' flag. *Warning
		if synchronizing or using a very short delay there maybe many verbose outputs in a short
		time.
	
`

var Subscribe = cli.Command{
	Name:        "subscribe",
	Usage:       "subscribe listens for and downloads the latests blocks",
	Description: _subscribeDescription,
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:  "receipts",
			Usage: "requests and save receipts for transactions",
		},
		&cli.BoolFlag{
			Name:    "connect",
			Usage:   "connects HEAD to a known ancestor, by downloading the missing blocks, should be true if delay is greater than average block time",
			EnvVars: []string{"HH_CONNECT"},
		},
		&cli.IntFlag{
			Name:    "delay",
			Usage:   "delay in seconds between requests for latest block",
			Value:   30,
			EnvVars: []string{"HH_DELAY"},
		},
		&cli.BoolFlag{
			Name:    "sync",
			Usage:   "ensures you gather missing blocks",
			EnvVars: []string{"HH_SYNC"},
		},
		&cli.BoolFlag{
			Name:  "verbose",
			Usage: "show all log outputs",
		},
		&cli.BoolFlag{
			Name:  "silent",
			Usage: "silences all logs",
		},
		&cli.BoolFlag{
			Name:  "no-panic",
			Usage: "recover if there is an unexpected error while subscribing",
		},
	},
	Action: _subscribe,
}

func _subscribe(c *cli.Context) error {

	// set logging options
	if !c.Bool("verbose") {
		gather.Log.SetFlags(0)
		gather.Log.SetOutput(ioutil.Discard)
	}
	if c.Bool("silent") {
		gather.Log.SetFlags(0)
		gather.Log.SetOutput(ioutil.Discard)
		gather.SyncLog.SetFlags(0)
		gather.SyncLog.SetOutput(ioutil.Discard)
		subscribe.Log.SetFlags(0)
		subscribe.Log.SetOutput(ioutil.Discard)
	}

	run(c, c.Bool("no-panic"))
	return nil
}

func run(c *cli.Context, noPanic bool) {
	defer func() {
		if noPanic {
			if r := recover(); r != nil {
				log.Println("no-panic: Recovered from err", r)
				run(c, noPanic)
			}
		}
	}()

	// create gather options
	opts := &gather.Options{
		DB:                helpers.DB(c.String("db")),
		Client:            helpers.Client(c.String("rpc")),
		GetAncestors:      c.Bool("connect"),
		GetReceipts:       c.Bool("receipts"),
		ShouldSynchronize: c.Bool("sync"),
		Schema:            schema.Get(c.String("schema")),
	}

	// create sub options
	subOpts := &subscribe.Options{
		Client:        opts.Client,
		Delay:         time.Second * time.Duration(c.Int("delay")),
		GatherOptions: opts,
	}

	// run
	subscribe.HTTP(subOpts)

}
