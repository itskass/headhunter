package helpers

import (
	"log"
	"os"
	"os/signal"
)

func HandleShutdown() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		// cleanup
		log.Println("Shutting down")
		os.Exit(1)

	}()
}
