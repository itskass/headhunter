package schema

import (
	"log"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/itskass/headhunter/schema/compact"
	"gitlab.com/itskass/headhunter/schema/scattered"
)

func Get(name string) Schema {
	switch name {
	case "compact":
		return &compact.Schema{}
	case "scattered":
		return &scattered.Schema{}
	}
	log.Fatal("unknown schema:`", name, "`, instead try `compact` or `scattered`")
	return nil
}

type Schema interface {
	Save(*types.Block, *mgo.Database) error
	Owned(bson.M, *mgo.Database) bool
	SaveReceipts([]*types.Receipt, *mgo.Database) error
}
