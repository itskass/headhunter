package compact

import (
	"github.com/ethereum/go-ethereum/core/types"
)

type Receipt struct {
	PostState []byte `json:"root"`
	Status    uint64 `json:"status"`
	//Logs              []*Log `json:"logs"              gencodec:"required"`

	// Implementation fields: These fields are added by geth when processing a transaction.
	// They are stored in the chain database.
	Hash            string `json:"transactionHash" gencodec:"required"`
	ContractAddress string `json:"contractAddress"`
	GasUsed         uint64 `json:"gasUsed" gencodec:"required"`

	// Inclusion information: These fields provide information about the inclusion of the
	TransactionIndex uint `json:"transactionIndex"`
}

func NewReceipt(r *types.Receipt) *Receipt {

	return &Receipt{
		PostState:        r.PostState,
		Status:           r.Status,
		GasUsed:          r.GasUsed,
		ContractAddress:  r.ContractAddress.String(),
		Hash:             r.TxHash.String(),
		TransactionIndex: r.TransactionIndex,
	}
}
