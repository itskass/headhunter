package compact

import (
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Schema struct{}

func (s *Schema) Save(block *types.Block, db *mgo.Database) error {
	b := NewBlock(block)
	_, err := db.C("blocks").Upsert(bson.M{"hash": b.Hash}, b)
	return err
}

func (s *Schema) Owned(query bson.M, db *mgo.Database) bool {
	n, _ := db.C("blocks").Find(query).Count()
	return n > 0
}

func (s *Schema) SaveReceipts(receipts []*types.Receipt, db *mgo.Database) error {
	for i := 0; i < len(receipts); i++ {
		r := NewReceipt(receipts[i])
		_, err := db.C("receipts").Upsert(bson.M{"hash": r.Hash}, r)
		if err != nil {
			return err
		}
	}
	return nil
}
