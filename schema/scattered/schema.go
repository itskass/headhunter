package scattered

import (
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Schema struct{}

func (s *Schema) Save(block *types.Block, db *mgo.Database) error {
	hash := block.Hash().String()
	txns := parseTransactions(block.Transactions(), block.Header())
	header := NewHeader(block.Header(), len(txns))

	_, err := db.C("headers").Upsert(bson.M{"hash": hash}, header)
	if err != nil {
		return err
	}

	for _, tx := range txns {
		_, err = db.C("transactions").Upsert(bson.M{"hash": tx.Hash}, tx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Schema) Owned(query bson.M, db *mgo.Database) bool {
	// make compact queries compatable:
	if num, ok := query["header.number"]; ok {
		delete(query, "header.number")
		query["number"] = num
	}

	n, _ := db.C("headers").Find(query).Count()
	return n > 0
}

func (s *Schema) SaveReceipts(receipts []*types.Receipt, db *mgo.Database) error {
	for i := 0; i < len(receipts); i++ {
		r := NewReceipt(receipts[i])
		_, err := db.C("receipts").Upsert(bson.M{"hash": r.Hash}, r)
		if err != nil {
			return err
		}
	}
	return nil
}
