package scattered

import (
	"github.com/ethereum/go-ethereum/core/types"
)

func parseTransactions(transactions []*types.Transaction, header *types.Header) []*Txdata {
	var txs []*Txdata
	blockHash := header.Hash().String()
	blockNum := header.Number.Uint64()
	for _, tx := range transactions {
		txs = append(txs, NewTxData(tx, blockHash, blockNum))
	}
	return txs
}
