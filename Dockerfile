FROM golang:1.13-alpine AS builder
# we require git
RUN apk update && apk add --no-cache git
RUN apk add build-base
# copy project files
WORKDIR /go/src/gitlab.com/itskass/headhunter
COPY . .
# run project files
RUN go get -d -v 
RUN go build -o hh

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/itskass/headhunter/hh .
ENTRYPOINT ["/root/hh"]