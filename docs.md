# Headhunter

**Usage**:

```
HEADHunter [GLOBAL OPTIONS] command [COMMAND OPTIONS] [ARGUMENTS...]
```

# GLOBAL OPTIONS

**--db**="": url of running MongoDB instance (default: localhost:27017)

**--help, -h**: show help

**--rpc**="": url of blockchain ethereum rpc (default: localhost:8545)

**--schema**="": the schema defines how blocks will be stored in database [compact, scattered] (default: scattered)

**--version, -v**: print the version


# COMMANDS

## gather

The gather command is often used to gather specific blocks or to synchronize a the 
blockchain up to a given point.

- GATHER TARGET:<br>
	The target is the block(range) gather will download. If no target is provided gather will
	will retrieve the latest block, otherwise you should provide either a number, number range
	or hash (as hexidecimal). e.g. '--number 55' will retrieve block 55.

- SYNCHRONIZING:<br>
	Synchronization will find and download all missing blocks up to the target. If you provided
	a target block with number 31, then all missing blocks between 0-31 will be downloaded.

- LOGGING:<br>
	HEADHunter aims to only log relevant information based on your usage, but you can silence 
	most logs with the '--silent' flag or show more logs with the '--verbose' flag.

FLAGS:

**--connect**: connects target to known ancestor, by downloading the missing blocks

**--hash**="": target block via hash

**--number**="": target block by number (index) (default: 0)

**--range**="": target blocks in the given range <start:end>

**--silent**: silences all logs

**--sync**: gather missing blocks

**--verbose**: show all log outputs when synchronizing

## subscribe
Subscribe is used to listen for and download the latested announced headers.

- UPDATE FREQUENCY:<br>
	By default subscribe will request the latest block from the connected node once every
	30 seconds, you can use the '--delay' flag to change this update period.

- PREVENTING GAPS:<br>
	When using HTTP RPC, subscribe will make a request for the latest block periodically,
	Use the '--connect' flag to retrieve all the blocks mined since the last periodic request.
	Without the '--connect' flag you will only recieve the most recent block in the period and 
	are likely to miss intermediate blocks in the sequence. 

- SYNCHRONIZING:<br>
	Synchronization will find and download all missing blocks up to the latest block.
	To ensure you download the complete blockchain and keep it updated. use the '--sync' flag 
	with and the '--connect' flag. e.g. 'subscribe --connect --sync'

- LOGGING:<br>
	HEADHunter aims to only log relevant information based on your usage, but you can silence 
	most logs with the '--silent' flag or show more logs with the '--verbose' flag. *Warning
	if synchronizing or using a very short delay there maybe many verbose outputs in a short
	time.
	
FLAGS:

**--connect**: connects HEAD to a known ancestor, by downloading the missing blocks, should be true if delay is greater than average block time

**--delay**="": delay in seconds between requests for latest block (default: 30)

**--silent**: silences all logs

**--sync**: ensures you gather missing blocks

**--verbose**: show all log outputs

## docs

generate documentation for HEADHunter

**--help, -h**: show help

**--out**="": path to file to save output in.

## help, h

Shows a list of commands or help for one command
