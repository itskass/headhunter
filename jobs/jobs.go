package jobs

import (
	"log"

	"github.com/globalsign/mgo"
	"github.com/robfig/cron"
)

type Job interface {
	Run(*mgo.Database) error
}

type JobOptions struct {
	DB *mgo.Database
	// Once if true the job be run only once
	Once bool
	// Job is the job to be run
	Jobs []Job
	// Delay how often this job runs
	Cron string
}

func Run(opts *JobOptions) {
	c := cron.New()
	for i := 0; i < len(opts.Jobs); i++ {
		c.AddFunc(opts.Cron, func() {
			err := opts.Jobs[i].Run(opts.DB)
			if err != nil {
				log.Println("job: err:", err)
			}
		})
	}
	c.Start()
}
