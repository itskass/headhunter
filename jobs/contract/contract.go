package contract

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/itskass/headhunter/schema/scattered"
)

type Job struct{}

type Contract struct {
	Address       string
	Type          string
	Deployer      string
	ConstructorTx string
	Code          string
}

func (j *Job) Run(db *mgo.Database) error {
	var txnData []*scattered.Txdata
	err := db.C("transactions").Find(bson.M{"recipient": ""}).All(&txnData)
	if err != nil {
		return err
	}

	for _, tx := range txnData {

	}

	return nil
}
