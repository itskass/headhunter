package gather

import (
	"context"
	"log"
	"math/big"

	"gitlab.com/itskass/headhunter/schema/compact"

	"github.com/ethereum/go-ethereum/core/types"

	"github.com/globalsign/mgo/bson"

	"gitlab.com/itskass/headhunter/schema"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/globalsign/mgo"
)

type Options struct {
	// Target of this gather, target can be a hash
	// block number or "latest".
	// REQUIRED
	Target bson.M
	// Client is used to communicate with the connected
	// nodes rpc.
	// REQUIRED
	Client *ethclient.Client
	// DB is used to store gathered blocks
	// REQUIRED
	DB *mgo.Database
	// ShouldSynchronize if true will download missing blocks
	// using provided sync options.
	ShouldSynchronize bool
	// SyncOptions contain head and tail values to synchronize
	// between
	SyncOptions SyncOptions
	// GetAncestors if true, the target blocks ancestors will be
	// gathered recursively.
	GetAncestors bool
	// Schema is responsible for saving the block into the
	// database usin a given scheme
	Schema schema.Schema
	// GetReceipts if true, the transaction reciepts are request
	// and stored.
	GetReceipts bool
}

func Blocks(opts *Options) {

	var (
		blockData *types.Block
		err       error
		target    = opts.Target
		c         = opts.Client
	)

	// request the block with this hash via rpc
	if _hash, ok := target["hash"]; ok {
		hash := _hash.(string)
		Log.Println("rpc: requesting block by hash", hash)
		if blockData, err = blockByHash(hash, c); err != nil {
			log.Println("rpc: err:", err)
			return
		}
	}

	// or request by number via rpc
	if _number, ok := target["number"]; ok {
		number := _number.(uint64)
		Log.Println("rpc: request block by number", number)
		if blockData, err = blockByNumber(number, c); err != nil {
			log.Println("rpc: err", err)
			return
		}
	}

	if _, ok := target["latest"]; ok {
		Log.Println("rpc: request latest block")
		blockData, err = c.BlockByNumber(context.Background(), nil)
		if err != nil {
			log.Println("rpc: err", err)
			return
		}
	}

	// get transaction recipets

	// convert block to mongodb friendly schema block
	// which uses hex values for hashes, addresses and
	// big.Ints
	block := compact.NewBlock(blockData)

	// save block to database
	err = opts.Schema.Save(blockData, opts.DB)
	if err != nil {
		log.Println("db: err:", err)
		return
	}
	Log.Println("db: saved block", block.Hash)

	// get receips
	if opts.GetReceipts {
		receipts := requestReceipts(block, c)
		err := opts.Schema.SaveReceipts(receipts, opts.DB)
		if err != nil {
			log.Println("db: reciepts err:", err)
		}
	}
	Log.Println("db: receipts saved", block.Hash)

	// get ancestors by getting ancestors if
	// ancestor exists
	if opts.GetAncestors {
		if !hasAncestor(block, opts) {
			Log.Println("sync: complete: no more missing ancestors")
			return
		}
		Log.Println("sync: unsynced ancestor found, requesting...")
		opts.Target = bson.M{"hash": block.Header.ParentHash}
		Blocks(opts)
	}

	// synchronize
	if opts.ShouldSynchronize {
		// disable only need to synchronize once
		opts.ShouldSynchronize = false
		// default sync options then synchronize from
		// current block to root
		if opts.SyncOptions.Head == 0 {
			opts.SyncOptions.Head = block.Header.Number
			opts.SyncOptions.Tail = 0
		}
		// Synchronize will download blocks between
		// the provided tail and head.
		Synchronize(opts)
	}
}

func hasAncestor(b *compact.Block, opts *Options) bool {
	// check if block is genesis
	if b.Header.Number < 1 {
		return false
	}

	// check if blocks parent is already owned
	return !opts.Schema.Owned(bson.M{"hash": b.Header.ParentHash}, opts.DB)
}

func blockByHash(hash string, c *ethclient.Client) (*types.Block, error) {
	_hash := common.HexToHash(hash)
	return c.BlockByHash(
		context.Background(),
		_hash,
	)
}

func blockByNumber(number uint64, c *ethclient.Client) (*types.Block, error) {
	_number := new(big.Int).SetUint64(number)
	return c.BlockByNumber(
		context.Background(),
		_number,
	)
}

func requestReceipts(block *compact.Block, c *ethclient.Client) []*types.Receipt {
	txns := block.Transactions
	var receipts []*types.Receipt
	for i := 0; i < len(txns); i++ {
		receipt, err := c.TransactionReceipt(context.Background(), common.HexToHash(txns[i].Hash))
		if err != nil {
			log.Println("rpc: receipt err:", err)
		} else {
			receipts = append(receipts, receipt)
		}
	}
	return receipts
}
