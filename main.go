package main

import (
	"io/ioutil"

	"gitlab.com/itskass/headhunter/helpers"

	"gitlab.com/itskass/headhunter/cmds"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "HEADHunter"
	app.Version = VERSION()
	app.Authors = []*cli.Author{
		{
			Name:  "kassius Barker",
			Email: "itskass94@gmail.com",
		},
	}
	app.Description =
		`HEADHunter is a tool for downloading/syncing ethereum-like blockchains to MongoDB via a nodes rpc.
		This software also provides some tools, quering blocks in the database.`

	// global flags
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "db",
			Usage:   "url of running MongoDB instance",
			Value:   "localhost:27017",
			EnvVars: []string{"HH_DBURL"},
		},
		&cli.StringFlag{
			Name:    "rpc",
			Usage:   "url of blockchain ethereum rpc",
			Value:   "localhost:8545",
			EnvVars: []string{"HH_RPCURL"},
		},
		&cli.StringFlag{
			Name:    "schema",
			Usage:   "the schema defines how blocks will be stored in database [compact, scattered]",
			Value:   "scattered",
			EnvVars: []string{"HH_SCHEMA"},
		},
	}

	// register commands
	app.Commands = []*cli.Command{
		&cmds.Gather,
		&cmds.Subscribe,
		&cmds.Docs,
	}

	// run application
	helpers.HandleShutdown()
	app.RunAndExitOnError()
}

func VERSION() string {
	buf, _ := ioutil.ReadFile("VERSION")
	return string(buf)
}
